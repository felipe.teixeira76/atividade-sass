<!DOCTYPE html>
<html lang-"pt-br">
<head>
    <meta charset="utf-8">
    
    <title>Atividade Proposta de Sass</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css">

</head>
<body>
    <div class="container mt-5">
        <div class="row">
            <div class="col">
                <nav id="menuTopo">
                     <ul>
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Empresa</a></li>
                        <li><a href="#">Produtos</a></li>
                        <li><a href="#">Serviços</a></li>
                        <li><a href="#">Contato</a></li>
                     </ul>
                </nav>
            </div>
        </div>

    <div>
    <div class="row">
        <div class="col text-center">
            <p> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            <a href="#" class="botaoprimario">Cadastrar</a>
            <a href="#" class="botaosecundario">Editar</a>
           </div>
        </div>
    </div>



    <script src="js/app.js"></script>
    <script src="js/jquery.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>

</html>